<?php

/**
 * User Information Form.
 *
 * @file
 * contains \Drupal\file_utility\Form\UserInformationForm
 */
namespace Drupal\file_utility\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;

/**
 * UserInformationForm class.
 */
class UserInformationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_information_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#prefix'] = '<div id="user_information">';
    $form['#suffix'] = '</div>';

    // The status messages that will contain any form errors.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $config = $this->config('file_utility.user_info_form');

    $form['name'] = [
      '#type' => 'textfield',
      '#name' => 'name',
      '#title' => $this->t('Name'),
      '#default_value' => $config->get('name'),
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#name' => 'email',
      '#title' => $this->t('Email Address'),
      '#default_value' => $config->get('email'),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions'
    ];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitUserFormAjax'],
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitUserFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $value = $form_state->getValues();
    $name = $value['name'];
    $email = $value['email'];
    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#user_information', $form));
    }
    else {
      $fields  = array(
        'name'   =>  'Test',
        'email' =>  'test@gmail.com',
        'file_path' => 'http://localhost/drupal_8_6_1/form/file_download_form',
        'created' =>  time(),
      );
      $query = \Drupal::database();
      $query->insert('file_downbload_users')
         ->fields($fields)
         ->execute();
      // drupal_set_message("succesfully saved");
      // $response = new RedirectResponse("/mydata/hello/table");
      // $response->send();
      $response->addCommand(new OpenModalDialogCommand("Success!", 'The user information has been submitted.', ['width' => 800]));
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('email');
    $is_valid_email = \Drupal::service('email.validator')->isValid($email);
    if (empty($is_valid_email)) {
      $form_state->setErrorByName('email', $this->t('Please enter valid Email Address.'));
    }
  }

  /**
   * Submit handler of the config Form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    //$config = $this->config('file_utility.UserInformationForm');
    // $values = $form_state->getValues();
    // $config->set('name', $values['name'])
    //   ->set('email', $values['email'])
    //   ->save();
    // parent::submitForm($form, $form_state);
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  // protected function getEditableConfigNames() {
  //   return ['user_information_form'];
  // }
}